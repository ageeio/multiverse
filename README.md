# multiverse

A free and open source desktop client for KoGaMa based on Electron.

## How to Install

To run the most current version of the application you must use this repository. The software might be unstable.

1. ``npm install -g electron-forge``

2. ``git clone https://gitlab.com/ageeio/multiverse.git``

3. ``cd multiverse``

4. ``npm install``

5. Run it using ``electron-forge start``

## How to Build

- To build the client for your own system, run ``electron-forge make``