import DiscordRPC = require('discord-rpc');

const clientId = '496480341385674772';

const rpc = new DiscordRPC.Client({ transport: 'ipc' });
const startTimestamp = new Date();

const statuses  = ["Playing", "Building"];

function status() {
    let url = webv.getURL();
    return({
	details: 'KoGaMa Friends',
	state: 'Pwning nubs',
	startTimestamp,
	largeImageKey: 'app_image',
	smallImageKey: 'status_game',
	instance: false,
    });
}

async function setActivity() {
    if (!rpc || !mainWindow) {
	return;
    }
    
    rpc.setActivity(status());
}

rpc.on('ready', () => {
    setActivity();
    setInterval(() => {
	setActivity();
    }, 15e3);
});

rpc.login({ clientId }).catch(console.error);
