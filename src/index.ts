import { app, BrowserWindow } from 'electron';
import installExtension, { REACT_DEVELOPER_TOOLS } from 'electron-devtools-installer';
import { enableLiveReload } from 'electron-compile';
import emoji = require('node-emoji');
import DiscordRPC = require('discord-rpc');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow: Electron.BrowserWindow | null = null;
let welcomeWindow: Electron.BrowserWindow | null = null;

const isDevMode = process.execPath.match(/[\\/]electron/);

if (isDevMode) {
  enableLiveReload({strategy: 'react-hmr'});
}

const createWindow = async () => {
    // Create the browser window.
    mainWindow = new BrowserWindow({
	width: 1200,
	height: 800,
	title: 'Multiverse',
    });
    
    /*welcomeWindow = new BrowserWindow({
	parent: mainWindow,
	modal: true,
	width: 300,
	height: 200,
	title: 'Dev Note',
    });*/

    mainWindow.setMenu(null);
    //welcomeWindow.setMenu(null);
  // and load the index.html of the app.
    mainWindow.loadURL(`file://${__dirname}/index.html`);
    /*welcomeWindow.loadURL(`file://${__dirname}/welcome.html`);
    */
    // Open the DevTools.
    if (isDevMode) {
	await installExtension(REACT_DEVELOPER_TOOLS);
	mainWindow.webContents.openDevTools();
  }
    
    // Emitted when the window is closed.
    mainWindow.on('closed', () => {
	// Dereference the window object, usually you would store windows
	// in an array if your app supports multi windows, this is the time
	// when you should delete the corresponding element.
	mainWindow = null;
    });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }


});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.

const clientId = '496480341385674772';

const rpc = new DiscordRPC.Client({ transport: 'ipc' });
const startTimestamp = new Date();

const statuses  = ["Browsing", "Playing", "Building"];

async function setActivity() {
    if (!rpc || !mainWindow) {
	return;
    }
    
    rpc.setActivity({
	details: 'KoGaMa Friends',
	state: 'Browsing',
	startTimestamp,
	largeImageKey: 'app_image',
	smallImageKey: 'server_friends',
	instance: false,
    });
}

rpc.on('ready', () => {
    setActivity();
    setInterval(() => {
	setActivity();
    }, 15e3);
});

rpc.login({ clientId }).catch(console.error);


catch(e) {
  console.log('Error:', e);
}



